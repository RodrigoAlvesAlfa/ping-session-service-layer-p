import { Module } from '@nestjs/common';
import { PingSessionModule } from './ping-session/ping-session.module';

@Module({
  imports: [PingSessionModule ],
  controllers: [],
  providers: [],
})
export class AppModule {}
