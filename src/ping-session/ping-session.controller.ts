import { Controller, Post, Body, Headers } from '@nestjs/common'
//import { ApiImplicitHeader } from '@nestjs/swagger';
import { LoginService } from './ping-session.service';

@Controller('ping-session')
export class PingSessionController {

  constructor(private readonly loginService: LoginService) { }

  @Post()
  //@ApiImplicitHeader({name: 'token', required: true})
  async session() {
    return this.loginService.login();
  }
  
}