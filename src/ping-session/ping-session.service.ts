import { Injectable } from '@nestjs/common';
//import { HttpService } from '../http/http.service';
import {api} from './api';
@Injectable()
export class LoginService {
  constructor() {}

  async login() {
    const dataRequest = {
      CompanyDB: '',
      Password: '',
      UserName: '',
    };

    const config = { headers: { 'Content-Type': 'application/json' } };

    const login = await api.post('Login', dataRequest, config).then((r) => {
      // if (r.error && r.error.innerMessage == 'Unauthorized') {
      //   r.error.message = 'Acesso não autorizado.';
      // }
      return r;
    });
    this.logout(login.data.SessionId)
    return login.data;
  }

  async logout(cookie: string) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: cookie,
      },
    };
    await api.post('/Logout', {}, config);
  }
}
