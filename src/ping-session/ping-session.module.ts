import { Module } from '@nestjs/common';
import { LoginService } from './ping-session.service';
import { PingSessionController } from './ping-session.controller';

@Module({
  imports: [],
  providers: [LoginService],
  controllers: [PingSessionController],
  exports: [LoginService],
})
export class PingSessionModule {}

